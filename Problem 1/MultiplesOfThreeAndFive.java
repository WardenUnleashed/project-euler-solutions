/*
By: Andrew Johnson
Solution to problem 1 of Project Euler.
https://projecteuler.net/problem=1
*/

public class MultiplesOfThreeAndFive{

   public static final int RANGE = 1000;
   
   public static void main(String[] args){
      System.out.println("The Sum of Multiples of Three or Five below " + RANGE + " is: " +  sumThreeFive());
   }
   
   public static int sumThreeFive(){
      int sum = 0;
      for(int x = 0; x < RANGE; x++){
         if(isMultiple(x,3) || isMultiple(x,5)){
            System.out.println(x + " is a multiple of 3 or 5");
            sum = sum + x;
         } else {
            System.out.println(x + " is NOT a multiple of 3 or 5");  
         } 
      }
      return sum;
   }
   
   public static boolean isMultiple(int number, int multiple){
      return (number % multiple) == 0;
      
   }
}